import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModeloDatos {

    private final static Logger LOGGER = Logger.getLogger("bitacora.subnivel.Control");

    private Connection con;
    private Statement set;
    private ResultSet rs;

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;
            con = DriverManager.getConnection(url, dbUser, dbPass);

        } catch (Exception e) {
            // No se ha conectado
            LOGGER.log(Level.SEVERE, "No se ha podido conectar");
            LOGGER.log(Level.SEVERE, "El error es: " + e.getMessage());
        }
    }

    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla
            LOGGER.log(Level.SEVERE, "No lee de la tabla");
            LOGGER.log(Level.SEVERE, "El error es: " + e.getMessage());
        }
        return (existe);
    }

    public void actualizarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre" + " LIKE '%" + nombre + "%'");
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            LOGGER.log(Level.SEVERE, "No modifica de la tabla");
            LOGGER.log(Level.SEVERE, "El error es: " + e.getMessage());
        }
    }

    public void insertarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            LOGGER.log(Level.SEVERE, "No inserta en la tabla");
            LOGGER.log(Level.SEVERE, "El error es: " + e.getMessage());
        }
    }

    public void resetearVotos(){
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=0");
            rs.close();
            set.close();
        } catch (Exception e) {
            //No se ha actualizado los votos de la base de datos
            LOGGER.log(Level.SEVERE, "No actualiza votos en la base de datos");
            LOGGER.log(Level.SEVERE, "El error es: " + e.getMessage());
        }
    }
    public ArrayList<Object> votosJugadores() {

        ArrayList<Object> resultado = new ArrayList<Object>();
        
        try {
        
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");

            HashMap<String,String> auxJugador = new HashMap<String,String>();
            while(rs.next()){
                auxJugador.put("nombre", rs.getString("nombre"));
                auxJugador.put("votos",(String) rs.getString("votos"));
                
                resultado.add(auxJugador);
                auxJugador = new HashMap<String,String>();
            }

            rs.close();
            set.close();


        } catch (Exception e) {
            //No recoge datos de la tabla
            LOGGER.log(Level.SEVERE, "No recoge datos de la tabla");
            LOGGER.log(Level.SEVERE, "El error es: " + e.getMessage());
        }

        return resultado;

    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "El error es: " + e.getMessage());
        }
    }

}
