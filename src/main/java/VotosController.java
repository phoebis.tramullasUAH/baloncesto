
import java.io.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.util.ArrayList;

public class VotosController extends HttpServlet {

    private ModeloDatos bd;


    @Override
    public void init() throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        ServletContext sc = this.getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/Votos.jsp");
        
        ArrayList<Object> resultado = bd.votosJugadores();
        req.setAttribute("datosVotos", resultado);
        
        rd.forward(req, res);
    }

    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
