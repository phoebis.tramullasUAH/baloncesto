import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class BorrarVotos extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        bd.resetearVotos();

        // Volvemos a cargar la pagina de inicio
        res.sendRedirect("/Baloncesto/");
    }

    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}