<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<!DOCTYPE html>
<html lang="es >
    <head>
        <title>Votaci&oacute;n mejor jugador liga ACB</title>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <font size=10>
        Votaci&oacute;n al mejor jugador de la liga ACB
        <hr>
        </font>
        <ul>
            <li class="table-header">
                <div class="col col-1">Nombre</div>
                <div class="col col-2">Nº Votos</div>
            </li>
                <% ArrayList<Object> datosVotos = (ArrayList<Object>)request.getAttribute("datosVotos");
                   for(int i = 0;i<datosVotos.size();i++){
                       HashMap<String,String> aux_voto = (HashMap<String,String>) datosVotos.get(i);
                       out.print("<li class=\"table-row\">");
                       out.print("<div class=\"col col-1\">"+aux_voto.get("nombre")+"</div>");
                       out.print("<div class=\"col col-2 votos-jugador\" id=\"voto-"+aux_voto.get("nombre")+"\">"+aux_voto.get("votos")+"</div>");
                       out.print("</li>");
                }%>
            
        </ul>
        <br>
        <br> <a href="index.html"> Ir al comienzo</a>
    </body>
</html>
