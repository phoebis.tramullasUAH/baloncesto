import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class ModeloDatosTest {

    //Objectos de la clase a probar
    private static ModeloDatos instance;


    @BeforeAll
    public static void setUp() {
        instance = new ModeloDatos();
    }

    @Test
    public void testExisteJugador() {
        System.out.println("Prueba de existeJugador");
        String nombre = "";
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
        //fail("Fallo forzado.");
    }

    @Test
    public void testActualizarJugador() throws SQLException {
        System.out.println("Prueba de actualizacion de votos del jugador");

        //Preparamos los mocks de las dependencias
        Connection conMock = Mockito.mock(Connection.class);
        Statement setMock = Mockito.mock(Statement.class);
        
        when(conMock.createStatement()).thenReturn(setMock);
        when(setMock.executeUpdate(anyString())).thenReturn(0);

        //Colocamos el mock en el atributo de la clase con la conexion de la base de datos
        Whitebox.setInternalState(instance, "con", conMock);

        instance.actualizarJugador("Carroll");

        //Comprobamos que en la funcion se han hecho las llamadas necesarias para actualizar la base de datos.
        verify(conMock, times(1)).createStatement();
        verify(setMock, times(1)).executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre LIKE '%Carroll%'");
    }
}