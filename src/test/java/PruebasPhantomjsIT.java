
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
public class PruebasPhantomjsIT
{
    private static WebDriver driver=null;

    @BeforeAll
    public static void setUp() {

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

        driver = new PhantomJSDriver(caps);
    }

    @AfterAll
    public static void cleanUp() {
        driver.close();
        driver.quit();
    }


    @Test
    public void tituloIndexTest()
    {
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");

        System.out.println(driver.getTitle());

    }

    @Test
    public void pruebaAnnadirVoto()
    {
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        //Insertamos el nombre del nuevo y lo votamos
        String nombre = "Gasol";
        driver.findElement(By.name("txtOtros")).click();
        driver.findElement(By.name("txtOtros")).sendKeys(nombre);
        //Seleccionamos la opcion otro y votamos
        driver.findElement(By.cssSelector("p:nth-child(12) > input:nth-child(1)")).click();
        driver.findElement(By.name("B1")).click();
        //Volvemos al home
        driver.findElement(By.linkText("Ir al comienzo")).click();
        //Nos aseguramos que el jugador se le ha annadido el voto
        driver.findElement(By.linkText("Ver votos")).click();
        assertThat(driver.findElement(By.id("voto-"+nombre)).getText(), is("1"));

    }

    @Test
    public void pruebaVotosACero()
    {
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        
        //Seleccionamos el boton para poner todos los votos a 0
        driver.findElement(By.cssSelector("form > input")).click();

        //Seleccionamos la opcion para ver los votos
        driver.findElement(By.linkText("Ver votos")).click();

        //Comprobamos que los elementos donde se muestran los votos estan todos a 0
        List<WebElement> voteElements = driver.findElements(By.className("votos-jugador"));

        for(int i=0; i<voteElements.size(); i++) {
            WebElement aux = voteElements.get(i);
            assertThat(aux.getText(), is("0"));
        }
        
    }
    
}